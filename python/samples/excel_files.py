import openpyxl
from openpyxl import Workbook
from openpyxl.styles import Color, PatternFill, Font, Border, Alignment, Side, Font
from openpyxl.styles import colors
import math
import unicodedata


class ExcelRow(object):
    def __init__(self):
        self.cells = [ExcelCell()]*0
        self.props = {}

    def __iter__(self):
        return self.cells.__iter__


class ExcelCell(object):
    def __init__(self, i_row=0, j_column=0, data_frame_name="", content="", props={}):
        self.name = data_frame_name
        self.content = content
        self.i_row = i_row
        self.j_column = j_column
        self.props = props


colors = {'baby blue': '00' + hex(197).replace("0x", "") + hex(217).replace("0x", "") + hex(241).replace("0x", ""),\
          'light grey': '00c9c9c9',\
          'spring green': '00b1ef81',\
          'red': '00FF0000',\
          'green': '0000FF00',\
          'blue': '000000FF'}


def get_col_from_index(idx):
    alphabet = "abcdefghijklmnopqrstuvw".upper()
    if idx < 0:
        return ""
    elif idx < len(alphabet):
        return alphabet[idx]
    else:
        return get_col_from_index(int(math.floor(float(idx-len(alphabet))/float(len(alphabet))))) + alphabet[idx % len(alphabet)]


def write_lines(filepath, lines=[ExcelCell()]*0, col_width=[]):
    wb = Workbook()
    # grab the active worksheet
    ws = wb.active
    # Rows can also be appended
    j = 1
    i = 1
    for l in lines:
        row = []
        for c in l:
            if type(c.content) is str:
                try:
                    content = unicodedata.normalize('NFKD', unicode(c.content, 'ascii', 'replace')).encode('ascii', 'ignore')
                except:
                    content = c.content
            else:
                content = c.content
            row.append(content)
        ws.append(row)

        # Style Cells
        for c in l:
            font = Font()
            if 'background color' in c.props.keys():
                clr = c.props['background color']
                if clr in colors.keys():
                    clr = colors[clr]
                bg = openpyxl.styles.colors.Color(rgb=clr)
                cell_bg = openpyxl.styles.fills.PatternFill(patternType='solid', fgColor=bg)
                ws.cell(i, j).fill = cell_bg
            if 'number format' in c.props.keys():
                ws.cell(i, j).number_format = c.props['number format']
            if 'text align' in c.props.keys():
                ws.cell(i, j).alignment = Alignment(horizontal=c.props['text align'])
            if 'bold text' in c.props.keys():
                font.bold = c.props['bold text']
            if 'italic text' in c.props.keys():
                font.italic = c.props['italic text']
            if 'border' in c.props.keys():
                borders = c.props['border']
                border = Border()
                if 'top' in borders.keys():
                    border.top = Side(style=borders['top'], color=borders['top color'])
                if 'bottom' in borders.keys():
                    border.bottom = Side(style=borders['bottom'], color=borders['bottom color'])
                if 'left' in borders.keys():
                    border.bottom = Side(style=borders['left'], color=borders['left color'])
                if 'right' in borders.keys():
                    border.bottom = Side(style=borders['right'], color=borders['right color'])
                ws.cell(i, j).border = border
            ws.cell(i, j).font = font
            j += 1
        i += 1
        j = 1
    j = 0
    for cw in col_width:
        if cw is not None:
            ws.column_dimensions[get_col_from_index(j)].width = cw
        j += 1
    wb.save(filepath)


def read_to_dict_of_lists(filepath, skipemptylines=False):
    '''
    :param filepath: of excel file
    :return:
    '''
    wb = openpyxl.load_workbook(filepath, read_only=True)
    print(wb.sheetnames)
    workbook = {}
    for s in wb.sheetnames:
        ws = wb[s]
        if not skipemptylines:
            workbook.update({s: [[c.value for c in row] for row in ws.rows]})
        else:
            rows = []
            for row in ws.rows:
                empty=True
                for c in row:
                    if c.value is not None:
                        empty=False
                        break
                if not empty:
                    rows.append([c.value for c in row])
            workbook.update({s: rows})
    return workbook


def write_from_dict_of_lists(dictoflists, targetexcel):
    '''
    :param dictoflists: {sheetname: [[row 1], [row 2], ...}
    :param targetexcel: filepath
    :return:
    '''
    wb = openpyxl.Workbook()

    for s in dictoflists.keys():
        wb.create_sheet(s)
        for row in dictoflists[s]:
            wb[s].append(row)
    wb.save(targetexcel)

