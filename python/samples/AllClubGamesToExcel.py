"""
Within the following example all games of a club
are gathered and print to an Excel file.

To use it, modify according to your needs:
- club_id (see readme how to get it)

Optional Step (not really needed, since SU obviously doesn't check it on server side...)
- replace the token in ".token" with your personal access token
    to the same directory as this file (or modify the script below). Get the token from Swissunihockey.

DEPENDENCIES:
    - openpyxl, get it by running 'py -m pip install openpyxl' in your command line
    - python 3
"""
from excel_files import ExcelCell, ExcelRow, write_lines
import swissunihockey_api_v2 as api


def all_games_to_excel(token, filepath, club_id=428, season=None):
    club = api.uhc = api.Club(id=428, token=token)
    games = club.get_all_games()
    props = {"bold text": True, "background color": "baby blue"}
    date_prop = {"number format": 'DD.MM.YYYY'}
    time_prop = {"number format": 'HH:MM'}
    rows = [[ExcelCell(content="Date", props={**props, **{"text align": "center"}}),
             ExcelCell(content="Kick-Off", props={**props, **{"text align": "center"}}),
             ExcelCell(content="League", props={**props, **{"text align": "center"}}),
             ExcelCell(content="Home", props={**props, **{"text align": "right"}}),
             ExcelCell(content="Away", props=props),
             ExcelCell(content="City", props=props),
             ExcelCell(content="Venue", props=props)]]
    for g in games:
        row = [ExcelCell(content=g.date, props={**{"text align": "center"}, **date_prop}),
               ExcelCell(content=g.time, props={**{"text align": "center"}, **time_prop}),
               ExcelCell(content=g.level, props={"text align": "center"}),
               ExcelCell(content=g.home_team.name, props={"text align": "right"}),
               ExcelCell(content=g.away_team.name),
               ExcelCell(content=g.location.city),
               ExcelCell(content=g.location.room)]
        rows.append(row)
    write_lines(filepath=filepath, lines=rows, col_width=[10]*2 + [35]*6)


if __name__ == "__main__":
    token = open(".token", "r").read()
    print("Starting to collect all games...")
    all_games_to_excel(token=token, filepath="AllClubGames.xlsx", club_id=428)
    print("Done!")

