import urllib.request
import json
import datetime
import traceback
import sys
import logging
LOGGER = logging.getLogger("SwissunihockeyAPIv2Wrapper")

if sys.version_info.major < 3:
    LOGGER.warning("Python 3 recommended")


class SwissunihockeyAPI(object):
    _BASE_URL = 'https://api-v2.swissunihockey.ch/api/'

    @staticmethod
    def get_response(url, is_json=True):
        if url.startswith("/"):
            url = url[1:]
        if not url.startswith("http"):
            url = SwissunihockeyAPI._BASE_URL + url
        request = urllib.request.Request(url)
        response = urllib.request.urlopen(request).read()
        if is_json:
            return json.loads(response.decode('utf-8'))
        else:
            return response

    @staticmethod
    def get_logo_by_team_id(team_id):
        team_details = SwissunihockeyAPI.get_response('teams/%d' % str(team_id))
        return team_details['data']['regions'][0]['rows'][0]['cells'][1]['image']['url']

    @staticmethod
    def get_ics_url(season=2019, team_id=0, club_id=428):
        return 'calendars?season=%d&team_id=%d&club_id=%d' % (season, team_id, club_id)

    @staticmethod
    def get_games_list_url(season=2020, team_id=0, club_id=None, mode="team", round=None):
        url = 'games?mode={MODE}&season={SEASON}&team_id={TEAM}{CLUBID}{ROUND}'
        return url.format(MODE=mode,
                          SEASON=season,
                          TEAM=team_id,
                          CLUBID= "&club_id=%d" % club_id if club_id is not None else "",
                          ROUND="&round=%d" % round if round is not None else "")

    @staticmethod
    def get_all_teams_url(season, club_id, auth_token):
        return 'teams?mode=by_club&season={SEASON}&club_id={CLUB}&auth_token={AUTH}'.format(SEASON=season,
                                                                                             CLUB=club_id,
                                                                                             AUTH=auth_token)

    @staticmethod
    def get_all_teams_url_no_auth(season, club_id):
        return 'teams?mode=by_club&season=%{SEASON}&club_id={CLUB}'.format(SEASON=season,
                                                                           CLUB=club_id)

    @staticmethod
    def get_all_teams_of_club(club_id, season=None, auth_token=None):
        if season is None:
            season = SwissunihockeyAPI.get_season()
        if auth_token is not None:
            response = SwissunihockeyAPI.get_response(url=SwissunihockeyAPI.get_all_teams_url(season=season,
                                                                                              club_id=club_id,
                                                                                              auth_token=auth_token), is_json=True)
        else:
            response = SwissunihockeyAPI.get_response(url=SwissunihockeyAPI.get_all_teams_url_no_auth(season=season,
                                                                                                      club_id=club_id), is_json=True)

        teams = []
        for item in response['entries']:
            id = item['set_in_context']['team_id']
            name = item['text']
            t = Team(name=name, id=id)
            t.update_all_info()
            teams.append(t)

        return teams
    
    @staticmethod
    def get_team_info_url(team_id):
        return 'teams/%d' % team_id
    
    @staticmethod
    def get_team_info(team_id):
        return SwissunihockeyAPI.get_response(url=SwissunihockeyAPI.get_team_info_url(team_id))
    
    @staticmethod
    def get_team_games_url(team_id, season=None):
        if season is None:
            season = SwissunihockeyAPI.get_season()
        return 'games?mode=team&season={SEASON}&team_id={TEAM}'.format(SEASON=season, TEAM=team_id)
    
    @staticmethod
    def get_team_games(team_id):
        return SwissunihockeyAPI.get_response(url=SwissunihockeyAPI.get_team_games_url(team_id))

    @staticmethod
    def get_game_details_url(game_id):
        return 'games/%d' % game_id
    
    @staticmethod
    def get_game_details(game_id):
        return SwissunihockeyAPI.get_response(url=SwissunihockeyAPI.get_game_details_url(game_id))

    @staticmethod
    def get_season():
        now = datetime.datetime.now()
        if now.month < 5:
            season = now.year - 1
        else:
            season = now.year
        return season


class Location(object):
    def __init__(self, room, city, coordinates):
        self.room = room
        self.city = city
        self.coordinates = coordinates

    def __repr__(self):
        return "%s, %s " % (self.room, self.city)


class SwissunihockeyDataObject(object):
    def __init__(self, name=None, id=None):
        self.name = name
        self.id = id

    def __repr__(self):
        return self.name


class Team(SwissunihockeyDataObject):
    _verbose = False

    def __init__(self, *args, clubname=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.clubname = clubname
        self.league_level = None

    def __repr__(self):
        if self.clubname is not None:
            return self.clubname
        else:
            return self.name

    def update_all_info(self):
        response = SwissunihockeyAPI.get_team_info(self.id)
        r = response["data"]["regions"][0]["rows"][0]["cells"]
        self.league_level = r[4]["text"][0]
        self.name = response["data"]["title"]

    def get_all_games(self, season=None):
        if season is None:
            season = SwissunihockeyAPI.get_season()
        games = []
        cnt = 0
        self.update_all_info()
        for p in range(1, 10):
            url = SwissunihockeyAPI.get_games_list_url(season=season, team_id=self.id, mode="team") + "&page=%d" % p
            try:
                resp = SwissunihockeyAPI.get_response(url=url, is_json=True)
                league = resp["data"]["tabs"][0]["text"] + (" %s" % self.league_level if self.league_level not in resp["data"]["tabs"][0]["text"] else "")
                for g in resp["data"]["regions"][0]["rows"]:
                    game_id = g["link"]["ids"][0]
                    game_row = g["cells"]
                    date = game_row[0]["text"][0]
                    time = game_row[0]["text"][1]
                    room = game_row[1]["text"][0]
                    city = game_row[1]["text"][1] if len(game_row[1]["text"]) > 1 else ""
                    coordinates = (game_row[1]["link"]["y"], game_row[1]["link"]["x"]) if "link" in game_row[1] else (0,0)
                    hometeamname = game_row[2]["text"][0]
                    awayteamname = game_row[3]["text"][0]
                    p = Game(home_team=Team(name=hometeamname, clubname=hometeamname),
                             away_team=Team(name=awayteamname, clubname=awayteamname),
                             date=date,
                             time=time,
                             score_home=None,
                             score_away=None,
                             game_detail_url=None,
                             location=Location(room=room, city=city, coordinates=coordinates),
                             game_id=game_id,
                             level=league)
                    games.append(p)
                    cnt += 1
                    Team._verbose_print("%2d. %s" % (cnt, p))
            except urllib.error.HTTPError as e:
                break
        return games

    @staticmethod
    def _verbose_print(*args, **kwargs):
        if Team._verbose:
            print(*args, **kwargs)


class League(SwissunihockeyDataObject):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class Game(object):
    def __init__(self, home_team=Team(), away_team=Team(), date=None, time=None, score_home=None,
                 score_away=None, game_detail_url=None, location=None, game_id=None, level=None):
        self.home_team = home_team
        self.away_team = away_team
        try:
            self.kickoff = datetime.datetime.strptime("%s %s" % (date, time), '%d.%m.%Y %H:%M')
        except Exception as e:
            self.kickoff = None
        self.score_home = score_home
        self.score_away = score_away
        self.detail_url = game_detail_url
        self.location = location
        self.game_id = game_id
        self.level = level

    def __repr__(self):
        return "%s %s %s vs. %s, %s" % (self.date, self.time, self.home_team, self.away_team, self.location)

    @property
    def date(self):
        return self.kickoff

    @property
    def time(self):
        return self.kickoff


class Club(SwissunihockeyDataObject):
    _verbose = False

    def __init__(self, *args, token, **kwargs):
        super().__init__(*args, **kwargs)
        self._token = token
        self.teams = SwissunihockeyAPI.get_all_teams_of_club(club_id=self.id, auth_token=self._token)

    @staticmethod
    def _verbose_print(*args, **kwargs):
        if Club._verbose:
            print(*args, **kwargs)

    def get_all_games(self, season=None, team=None):
        if season is None:
            season = SwissunihockeyAPI.get_season()
        games = []
        if team is None:
            team = self.teams
        for t in team:
            Club._verbose_print("******************************************************************")
            Club._verbose_print("* %s" % t.league_level)
            Club._verbose_print("******************************************************************")
            games.extend(t.get_all_games(season=season))
            Club._verbose_print(" ")
        return games