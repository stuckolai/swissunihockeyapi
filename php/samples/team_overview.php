<?php
include "../suapi_functions.php";
/********************************************************************************************************************/
/* Parameters   						                                                                            */
/********************************************************************************************************************/
$club_id = 428;
$club_name = "UHC Burgdorf"
$auth_token = '1234567890abcdef1234'; // Not sure whether really needed..?
$team_alias = array(
    "427489" => "Herren 1 GF",
    "428524" => "Herren 3 KF",
	"430770" => "Herren 2 GF",
	"429942" => "Senioren KF",
	"424238" => "U21 GF",
	"431171" => "U18 (U21) GF",
	"428890" => "U16 GF",
	"428113" => "Junioren C KF",
	"428117" => "Junioren D KF",
	"427531" => "Junioren E KF",
	"429127" => "Damen 1 KF",
	"429451" => "Damen 2 KF",
);


/********************************************************************************************************************/
/* Functions    						                                                                            */
/********************************************************************************************************************/
function p_html_echo($str){
	echo '<p>' . $str . '</p>';
}

function offline_team_overview($season){
	echo 'Swissunihockey seems to be down for the moment..';
}



try{
$now = time();
if (date("m", $now) > 6){
    $season = date("Y", $now);
}else{
    $season = date("Y", $now)-1;
};

$urlTeams = 'https://api-v2.swissunihockey.ch/api/teams?mode=by_club&season=' . strval($season) . '&club_id=' . strval($club_id) . '&auth_token=' . $auth_token;
$urlTeamDetails = 'https://www.swissunihockey.ch/de/team-detail?season=' . strval($season) . '&team_id=';

/* CATCH TEAMS OF CLUB */
$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => $urlTeams,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache"
  ),
));
$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
$response = json_decode($response, true);

$club_teams = array();
foreach($response['entries'] as $item) {
	$teamname = $item['text'];
	$team_id = $item['set_in_context']['team_id'];
	$t = New team;
	$t->id = $team_id;
	$t->name = $teamname;
	array_push($club_teams,$t);
}

/********************************************************************************************************************/
/* PRINT AS HTML						                                                                            */
/********************************************************************************************************************/
echo '<table style="width:100%">';
echo '<tr>';
echo '<td style="font-weight:bold" >' . $club_name . ' Team Name </td>';
echo '<td style="font-weight:bold" >Liga</td>';
echo '</tr>';
foreach($club_teams as $team) {
	$linkOpen = '<a href="' . $urlTeamDetails . $team->id .'" target="_blank" style="text-decoration: None;text-color: black">';
	$linkClose = '</a>';
	echo '<tr>';
	if (array_key_exists((string)$team->id, $team_alias)) {
        echo '<td>' . $team_alias[(string)$team->id] . '</td>';
    } else {
        echo '<td>' . (string)$team->name . '</td>';
    }

	echo '<td>' . $linkOpen . $team->name .  $linkClose . '</td>';
	echo '</tr>';

};
echo '</table>';
}catch(Exception $e){
	offline_team_overview($season);
}
?>
