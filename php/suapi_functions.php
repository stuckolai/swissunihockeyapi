<?php

include "floorball_functions.php"

//***********************************************************
//*	CLASSES & HELPERS
//***********************************************************
class team {
    public $name = '';
    public $su_id= 0 ;
} ;

class club_game {
    public $home = '';
	public $away = '';
	public $location = '';
	public $date = '';
	public $result= '';
    public $su_id= 0 ;
	public $level ='';
	public $is_home = false;
	
	function get_score_home() {
		$pos = strpos(strval($this->result), ':');
        return substr(strval($this->result),0,$pos);
    }
	
	function get_score_away() {
		$pos = strpos(strval($this->result), ':')+1;
        return substr(strval($this->result),$pos,strlen($this->result));
    }
} ;


function curl_request($url){
	$curl = curl_init();
	curl_setopt_array($curl, array(
	  CURLOPT_URL => $url,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
		"cache-control: no-cache"
	  ),
	));
	$response = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	return json_decode($response, true); 
}

//***********************************************************
//*	V2 SU API
//***********************************************************
$base_url='https://api-v2.swissunihockey.ch/api/';
$surl='https://www.swissunihockey.ch/'

function get_teams_url($auth_token, $club_id, $season){
	return $base_url . 'teams?mode=by_club&season=' . strval($season) . '&club_id=' . strval($club_id) . '&auth_token=' . $auth_token;
}
function get_games_of_club_url($auth_token, $club_id, $season){
	return $base_url . 'games?mode=club&season=' . (string)$season . '&club_id=' . (string)$club_id . '&auth_token=' . (string)$auth_token;
}
function get_game_detail_url($game_id, $language="de"){
	return $surl . $language . '/game-detail?game_id=' . $game_id;	
}
function get_team_details_url($season, $team_id, $language="de"){
	return $surl . $language . '/team-detail?season=' . $season . '&team_id=' . $team_id;
}


function get_all_teams_of_club($club_id, $token){
	$response = curl_request(get_teams_url($token, $club_id, get_season_start()));
	$club_teams = array();
	foreach($response['entries'] as $item) { 
		$teamname = $item['text'];
		$team_id = $item['set_in_context']['team_id'];
		$t = New team;
		$t->id = $team_id;
		$t->name = $teamname;
		array_push($club_teams,$t);
	}
	return $club_teams;
}



?>