<?php
/*

*/
function get_season_start() {
	if (date("m") > 5){
		$season = date("Y");
	}else{
		$season = date("Y")-1;
	};
	return $season;
}

/*

*/
function get_season_end() {
	return get_season_start() + 1;
}

/*

*/
function get_season_string() {
	return get_season_start() . "/" . get_season_end();
}

/*

*/
function get_junior_ages() {
	$season=get_season_start();
	$ref_year=2020;
	$oldest_2020 = array(
		"F" => 2013,
		"E" => 2011,
		"D" => 2009,
		"C" => 2007,
		"U14" => 2007,
		"U16" => 2005,
		"U18" => 2003,
		"U21" => 2000,
	);
	$ages=array();
	foreach($oldest_2020 as $key => $year) { 	
	    $oldest = $year + $season - $refyear
		$ages[$key]=$oldest;
	};
	return $ages;
}



?>