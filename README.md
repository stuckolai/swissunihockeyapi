# Swissunihockey API interface library for Python and PHP
For the moment it seems unclear to me which API version (1,2 or 3) they are really going for.
So far v2 seems to me the most reliable one (except for Sunday evenings, where the SU engine seems to be down on a 
weekly basis)


Official Documentation:
[Swissunihockey API v2 Documentation](https://api-v2.swissunihockey.ch/api/doc/table/overview)
[Statement by Swissunihockey](https://www.swissunihockey.ch/de/administration/services/webmaster/)


# QAs

## How to get my club's id?
Go to your clubs home-page on Swissunihockey. You can get there if you click on your club's name 
anywhere in the results/ game schedule on the Swiss-Unihockey page.
You'll happen to a URL something like:
```bash
https://www.swissunihockey.ch/de/team-detail?team_id=428890
```
The id equals the first (three?) letters of team_id, e.g. **428**890

## How to get an access token?
Not sure if really needed, not sure where to get. But write an E-mail to Swissunihockey..

## What is this repo useful for?
idk.. Made it to have all games in an excel table. This might be useful to you too.

## Can I contribute?
Sure, just let me know..